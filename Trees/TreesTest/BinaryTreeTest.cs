﻿using Trees;
using TreesTest.InterfaceTests;
using Xunit;

namespace TreesTest
{
    public class BinaryTreeTest
    {
        public BinaryTree<int> IntTree => new BinaryTree<int>();
        public BinaryTree<string> StringTree => new BinaryTree<string>();
        public BinaryTree<object> ObjectTree => new BinaryTree<object>();

        [Fact]
        public void TestTree() => BinaryTreeInterfaceTest.Test(IntTree, StringTree, ObjectTree);
    }
}
