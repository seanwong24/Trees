using System;
using Trees;
using Xunit;

namespace TreesTest.InterfaceTests
{
    public static class PrintableInterfaceTest
    {
        public static void Test(ITree<int> intTree, ITree<string> stringTree, ITree<object> objectTree)
        {
            TestInt(intTree);
            TestString(stringTree);
            TestObject(objectTree);
        }

        private static void TestObject(ITree<object> objectTree)
        {
            
        }

        private static void TestString(ITree<string> stringTree)
        {
            
        }

        private static void TestInt(ITree<int> intTree)
        {
            
        }
    }
}
