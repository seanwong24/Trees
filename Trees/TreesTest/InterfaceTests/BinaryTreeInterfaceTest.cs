using System;
using Trees;
using Xunit;

namespace TreesTest.InterfaceTests
{
    public static class BinaryTreeInterfaceTest
    {
        public static void Test(IBinaryTree<int> intTree, IBinaryTree<string> stringTree, IBinaryTree<object> objectTree)
        {
            TreeInterfaceTest.Test(intTree, stringTree, objectTree);

            TestInt(intTree);
            TestString(stringTree);
            TestObject(objectTree);
        }

        private static void TestObject(IBinaryTree<object> objectTree)
        {
            object o1 = DateTime.Now;
            object o2 = DateTime.Now.AddDays(2);
            objectTree.LeftData = o1;
            objectTree.RightData = o2;
            Assert.Equal(objectTree.LeftData, o1);
            Assert.Equal(objectTree.RightData, o2);
        }

        private static void TestString(IBinaryTree<string> stringTree)
        {
            string s1 = "hello world";
            string s2 = "Hello World";
            stringTree.LeftData = s1;
            stringTree.RightData = s2;
            Assert.Equal(stringTree.LeftData, s1);
            Assert.Equal(stringTree.RightData, s2);
        }

        private static void TestInt(IBinaryTree<int> intTree)
        {
            int i1 = 10086;
            int i2 = 1024;
            intTree.LeftData = i1;
            intTree.RightData = i2;
            Assert.Equal(intTree.LeftData, i1);
            Assert.Equal(intTree.RightData, i2);
        }
    }
}
