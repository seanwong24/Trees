using System;
using Trees;
using Xunit;

namespace TreesTest.InterfaceTests
{
    public static class TreeInterfaceTest
    {
        public static void Test(ITree<int> intTree, ITree<string> stringTree, ITree<object> objectTree)
        {
            PrintableInterfaceTest.Test(intTree, stringTree, objectTree);

            TestInt(intTree);
            TestString(stringTree);
            TestObject(objectTree);
        }

        private static void TestObject(ITree<object> objectTree)
        {
            object o = DateTime.Now;
            objectTree.Data = o;
            Assert.Equal(objectTree.Data, o);
        }

        private static void TestString(ITree<string> stringTree)
        {
            string s = "hello world";
            stringTree.Data = s;
            Assert.Equal(stringTree.Data, s);
        }

        private static void TestInt(ITree<int> intTree)
        {
            int i = 10086;
            intTree.Data = i;
            Assert.Equal(intTree.Data, i);
        }
    }
}
