﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Trees
{
    public class VirtualEnumerable<TData, TEnumerator> : IEnumerable<TData>
    {
        public IEnumerator<TData> GetEnumerator()
        {
            return Activator.CreateInstance<TEnumerator>() as IEnumerator<TData>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
