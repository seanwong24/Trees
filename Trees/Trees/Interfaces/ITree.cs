﻿using System.Collections.Generic;

namespace Trees
{
    public interface ITree<T> : IContainer<T>, IPrintable
    {
        T Data { get; set; }
        IEnumerable<ITree<T>> ChildNodes { get; }
    }
}
