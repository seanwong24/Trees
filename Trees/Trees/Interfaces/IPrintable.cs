﻿namespace Trees
{
    public interface IPrintable
    {
        void Print();
    }
}
