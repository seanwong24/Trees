﻿using System;
using System.Collections.Generic;

namespace Trees
{
    public interface ITraversable<T>
    {
        IEnumerable<T> AsEnumerable();
        IEnumerable<T> AsEnumerable<TEnumerator>();
    }
}
