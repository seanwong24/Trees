﻿using System;

namespace Trees
{
    public interface ISortable<T>
    {
        void Sort();

        void Sort(Predicate<T> determiner);
    }
}
