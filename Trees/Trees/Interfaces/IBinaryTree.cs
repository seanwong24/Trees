﻿namespace Trees
{
    public interface IBinaryTree<T> : ITree<T>
    {
        T LeftData { get; set; }
        T RightData { get; set; }
        IBinaryTree<T> LeftNode { get; }
        IBinaryTree<T> RightNode { get; }
    }
}
