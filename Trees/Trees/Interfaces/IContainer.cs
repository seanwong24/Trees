﻿using System;

namespace Trees
{
    public interface IContainer<T> : ITraversable<T>
    {
        bool IsEmpty { get; }
        bool IsFull { get; }

        void Clear();
        void Insert(T data);
        T Remove(Predicate<T> determiner);
    }
}
